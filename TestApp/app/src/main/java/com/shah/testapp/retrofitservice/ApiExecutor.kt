package `in`.bluejack.bidsapp.retrofitservice

import android.content.Context
import com.shah.testapp.utils.ApplicationConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiExecutor {

    private var baseUrl: String? = null
    private var retrofit: Retrofit? = null

    fun getApiService(mContext: Context): ApiService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()

        baseUrl = ApplicationConstants.BASE_URL
        retrofit = Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()

        return retrofit!!.create(ApiService::class.java)
    }

}