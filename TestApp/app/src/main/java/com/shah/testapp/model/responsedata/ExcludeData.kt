package `in`.bluejack.bidsapp.retrofitservice

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ExcludeData : Serializable{

    @SerializedName("group_id")
    var groupId: String? = null

    @SerializedName("variation_id")
    var variationId: String? = null

}