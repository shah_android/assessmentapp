package com.shah.testapp.adapter

import `in`.bluejack.bidsapp.retrofitservice.VariationData
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.shah.testapp.R

/**
 * Created by ravi.shah.
 */
class VariantsListAdapter() : RecyclerView.Adapter<VariantsListAdapter.ViewHolder>() {

    private lateinit var mContext: AppCompatActivity
    private lateinit var arlData: List<VariationData>

    constructor(mContext: AppCompatActivity, arlData: List<VariationData>) : this() {
        this.mContext = mContext;
        this.arlData = arlData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VariantsListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_variants_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: VariantsListAdapter.ViewHolder, position: Int) {
        holder.tvVariationName!!.text = arlData!!.get(position).name
        holder.tvPrice!!.text = "Price: "+ arlData!!.get(position).price

        if(arlData.get(position).isHeader!!){
            holder.tvVariantGroupName!!.visibility = View.VISIBLE
            holder.tvVariantGroupName!!.text = arlData.get(position).groupName
        }else{
            holder.tvVariantGroupName!!.visibility = View.GONE
            holder.tvVariantGroupName!!.text = ""
        }

        if(arlData.get(position).inStock.equals("1")){
            holder.tvInStock!!.text = "In Stock: Available"
        }else{
            holder.tvInStock!!.text = "In Stock: Unavailable"
        }

        if(arlData.get(position).isSelected!!){
            holder.rbItem!!.isChecked = true
        }else{
            holder.rbItem!!.isChecked = false
        }

        if(arlData.get(position).isExclude!!){
            holder.tvVariationName!!.setTextColor(Color.parseColor("#777777"))
            holder.rbItem!!.isEnabled = false
            holder.rbItem!!.isChecked = false
            holder.tvVariationName!!.isEnabled = false
            holder.tvPrice!!.isEnabled = false
            holder.tvInStock!!.isEnabled = false
        }else{
            holder.tvVariationName!!.setTextColor(Color.parseColor("#000000"))
            holder.rbItem!!.isEnabled = true
            holder.tvVariationName!!.isEnabled = true
            holder.tvPrice!!.isEnabled = true
            holder.tvInStock!!.isEnabled = true
        }

        holder.llMainView!!.setOnClickListener{
            if(!arlData.get(position).isExclude!!){
                for (i in arlData.indices){
                    if(arlData.get(i).groupId == arlData.get(position).groupId){
                        if(position == i){
                            arlData.get(position).isSelected = true
                        }else{
                            arlData.get(i).isSelected = false
                        }
                    }

                    if(arlData.get(i).id == arlData.get(position).excludeVariationId){
                            arlData.get(i).isExclude = true
                    }else{
                        if(arlData.get(i).groupId != arlData.get(position).groupId){
                            if(arlData.get(position).groupName == "Crust") {
                                arlData.get(i).isExclude = false
                            }else if(arlData.get(position).groupName == "Size") {
                                arlData.get(i).isExclude = false
                            }
                        }

                    }
                }
                notifyDataSetChanged()
            }
        }

        holder.rbItem!!.setOnClickListener{
            if(!arlData.get(position).isExclude!!){
                for (i in arlData.indices){
                    if(arlData.get(i).groupId == arlData.get(position).groupId){
                        if(position == i){
                            arlData.get(position).isSelected = true
                        }else{
                            arlData.get(i).isSelected = false
                        }
                    }

                    if(arlData.get(i).id == arlData.get(position).excludeVariationId){
                            arlData.get(i).isExclude = true
                    }else{
                        if(arlData.get(i).groupId != arlData.get(position).groupId){
                            if(arlData.get(position).groupName == "Crust") {
                                arlData.get(i).isExclude = false
                            }else if(arlData.get(position).groupName == "Size") {
                                arlData.get(i).isExclude = false
                            }
                        }

                    }
                }
                notifyDataSetChanged()
            }
        }


    }

    override fun getItemCount(): Int {
        return arlData!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rbItem: RadioButton? = null
        var tvVariantGroupName: TextView? = null
        var tvVariationName: TextView? = null
        var tvPrice: TextView? = null
        var tvInStock: TextView? = null
        var llMainView: LinearLayout? = null

        init {
            rbItem = itemView.findViewById(R.id.rbItem) as RadioButton
            llMainView = itemView.findViewById(R.id.llMainView) as LinearLayout
            tvVariantGroupName = itemView.findViewById(R.id.tvVariantGroupName) as TextView
            tvVariationName = itemView.findViewById(R.id.tvVariationName) as TextView
            tvPrice = itemView.findViewById(R.id.tvPrice) as TextView
            tvInStock = itemView.findViewById(R.id.tvInStock) as TextView
        }
    }

}