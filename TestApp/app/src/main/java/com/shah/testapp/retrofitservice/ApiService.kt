package `in`.bluejack.bidsapp.retrofitservice

import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("b/5d70aad4fc5937640ce3820d")
    abstract fun apiVariantGroupList(): Call<JsonResponse>


}