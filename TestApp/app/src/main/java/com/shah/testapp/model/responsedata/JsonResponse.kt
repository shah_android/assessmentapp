package `in`.bluejack.bidsapp.retrofitservice

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class JsonResponse: Serializable{

    @SerializedName("variants")
    var variants: VariantGroupData? = null
}