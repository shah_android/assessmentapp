package `in`.bluejack.bidsapp.retrofitservice

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GroupData : Serializable{

    @SerializedName("group_id")
    var groupId: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("variations")
    var variations: List<VariationData>? = null
}