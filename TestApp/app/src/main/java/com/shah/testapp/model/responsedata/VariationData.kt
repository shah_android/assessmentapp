package `in`.bluejack.bidsapp.retrofitservice

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VariationData: Serializable{

    @SerializedName("name")
    var name: String? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("default")
    var default: String? = null

    @SerializedName("id")
    var id: String? = null

    @SerializedName("inStock")
    var inStock: String? = null

    var groupId: String? = null
    var excludeVariationId: String? = ""
    var isHeader: Boolean? = false
    var isExclude: Boolean? = false
    var isSelected: Boolean? = false
    var groupName : String? = ""
}