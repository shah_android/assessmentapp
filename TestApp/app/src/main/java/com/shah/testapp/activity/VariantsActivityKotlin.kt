package com.shah.testapp.activity

import `in`.bluejack.bidsapp.retrofitservice.ApiExecutor
import `in`.bluejack.bidsapp.retrofitservice.JsonResponse
import `in`.bluejack.bidsapp.retrofitservice.VariantGroupData
import `in`.bluejack.bidsapp.retrofitservice.VariationData
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.shah.testapp.R
import com.shah.testapp.adapter.VariantsListAdapter
import com.shah.testapp.model.ExcludeCustomData
import com.shah.testapp.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VariantsActivityKotlin : AppCompatActivity() {

    private lateinit var tvHeader: TextView
    private lateinit var rvVariationList: RecyclerView
    private lateinit var mContext: AppCompatActivity
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var orderedListAdapter: VariantsListAdapter
    private lateinit var variantGroupData: VariantGroupData
    private lateinit var arlVariationList: ArrayList<VariationData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common_view)
        mContext = this;
        getIds()
        arlVariationList = ArrayList<VariationData>()
        variantGroupData = VariantGroupData()
        setRecyclerViewData()
        getVariantGroupList()
    }

    private fun getIds() {
        rvVariationList = findViewById<RecyclerView>(R.id.rvVariationList) as RecyclerView
        tvHeader = findViewById<TextView>(R.id.tvHeader) as TextView

    }

    private fun setRecyclerViewData() {
        layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rvVariationList!!.layoutManager = layoutManager
        orderedListAdapter = VariantsListAdapter(mContext, arlVariationList)
        rvVariationList!!.adapter = orderedListAdapter
    }

    private fun getVariantGroupList() {
        val call = ApiExecutor.getApiService(this).apiVariantGroupList()

        call.enqueue(object : Callback<JsonResponse> {
            override fun onResponse(call: Call<JsonResponse>, response: Response<JsonResponse>?) {
                println("VariantGroupActivityKotlin " + "API Data" + Gson().toJson(response!!.body()))

                if(response!!.body()!!.variants !=null ){
                    variantGroupData = response.body()!!.variants!!
                    arlVariationList = ArrayList<VariationData>()

                    if(variantGroupData.variantGroups!=null && variantGroupData.variantGroups!!.size>0){
                        for(i in variantGroupData.variantGroups!!.indices){
                            for(j in variantGroupData.variantGroups!!.get(i).variations!!.indices){
                                var variationData: VariationData? = VariationData()
                                variationData = variantGroupData.variantGroups!!.get(i).variations!!.get(j)
                                variationData!!.groupName = variantGroupData.variantGroups!!.get(i).name
                                variationData!!.groupId = variantGroupData.variantGroups!!.get(i).groupId
                                if(j == 0){
                                    variationData!!.isHeader = true
                                }else{
                                    variationData!!.isHeader = false
                                }
                                arlVariationList.add(variationData)
                            }
                        }

                        val excludeHashMap: HashMap<String, String> = hashMapOf()
                        val excludeData: ArrayList<ExcludeCustomData>
                        excludeData = ArrayList<ExcludeCustomData>()
                        for (i in variantGroupData.excludeList!!) {
                            val excludeCustomData: ExcludeCustomData? = ExcludeCustomData()
                            var count: Int? = 0
                            for (j in i) {
                                excludeHashMap[j.groupId!!] = j.variationId!!
                                if(count == 0){
                                    excludeCustomData!!.variantId = excludeHashMap[j.groupId!!]!!
                                }else if(count == 1) {
                                    excludeCustomData!!.excludeVariantId = excludeHashMap[j.groupId!!]!!
                                }
                                count = count!!+1
                            }
                            excludeData!!.add(excludeCustomData!!)
                        }

                        for (i in arlVariationList!!.indices) {
                            for (j in excludeData!!.indices) {
                                if(arlVariationList.get(i).id == excludeData.get(j).variantId){
                                    arlVariationList.get(i).excludeVariationId = excludeData.get(j).excludeVariantId
                                }
                            }
                        }

                        orderedListAdapter = VariantsListAdapter(mContext, arlVariationList)
                        rvVariationList!!.adapter = orderedListAdapter

                    }
                } else {
                    CommonUtils.showToast(mContext, getString(R.string.server_not_responding))
                }
            }

            override fun onFailure(call: Call<JsonResponse>, t: Throwable) {
                println("API Data Error : " + t.message)
            }
        }
        )
    }
}