package `in`.bluejack.bidsapp.retrofitservice

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VariantGroupData: Serializable{

    @SerializedName("variant_groups")
    val variantGroups: List<GroupData>? = null

    @SerializedName("exclude_list")
    val excludeList: List<List<ExcludeData>>? = null
}